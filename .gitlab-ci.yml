stages:
  # Build all artefacts that are used in the following jobs
  - pre_build
  # Build all artefacts that may be published or put to production such as packages or containers
  - build
  - test
  # Deploy to staging environment
  - staging
  # Deploy to production environment
  - deploy

workflow:
  rules:
    # A push event on a merge request should not be triggered to avoid duplicate pipelines
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_MERGE_REQUEST_ID != null'
      when: never
    # A merge request pipeline
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: always
    - when: always


variables:
  # The canonical name of the runtime image, that is used to build production and to
  # run some jobs. It is written without any tag.
  RUNTIME_CANONICAL_IMAGE_NAME: "damned-lies-runtime"
  # The runtime image hosted on GitLab
  RUNTIME_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/damned-lies-runtime:${CI_COMMIT_REF_NAME}-latest"


# A job that runs on the runtime image (the one that is used the build the
# production container should inherit from this template.
.job-on-runtime-image:
  image: "${RUNTIME_IMAGE_NAME}"


# To avoid power/cpu/memory consumption, some jobs (almost all) should not run on po file
# changes. This change is made using Damned Lies itself.
.do-not-run-on-po-change:
  rules:
    - changes:
        - po/*
      when: manual

.do-not-run-on-merge-request:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: never


# Do not run this job if it’s running on a branch different from the main branch
.do-not-run-if-not-on-main-branch:
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      when: never

# A job that builds an image using buildah should inherit from this template
.job-on-buildah:
  image: quay.io/gnome_infrastructure/buildah
  variables:
    # Specific to buildah
    STORAGE_DRIVER: vfs
    BUILDAH_FORMAT: docker


.build_damned_lies_image:
  extends: .job-on-buildah
  variables:
    # References for the build script
    CURRENT_VCS_REF_NAME: "${CI_COMMIT_REF_NAME}"
    BUILT_IMAGE_NAME: "damned-lies-production:${CI_COMMIT_REF_NAME}"
    IMAGE_TAG: "${CI_REGISTRY_IMAGE}/damned-lies-production:${CI_COMMIT_REF_NAME}"
    KIND_OF_DEPLOYMENT: "production"
  script:
    - dnf install -y python3-jinja2-cli git
    - ./containers/production/render_configuration_templates.sh "${KIND_OF_DEPLOYMENT}"
    - ./containers/production/build_buildah_deployment.sh "${KIND_OF_DEPLOYMENT}"
    - buildah tag "${BUILT_IMAGE_NAME}-${KIND_OF_DEPLOYMENT}" "${IMAGE_TAG}-${KIND_OF_DEPLOYMENT}"
    - buildah push "${IMAGE_TAG}-${KIND_OF_DEPLOYMENT}" "oci-archive:damned-lies-production_${CI_COMMIT_REF_NAME}.tar"
  after_script:
    - mv containers/production/${KIND_OF_DEPLOYMENT} configuration
  artifacts:
    name: "${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}"
    expose_as: "Container image"
    paths:
      - damned-lies-production_$CI_COMMIT_REF_NAME.tar
      - configuration
    expire_in: 3 hours
    when: on_success


##############################################################################
###################             Pre Build                  ###################
##############################################################################
build_damned_lies_runtime:
  extends: .job-on-buildah
  stage: pre_build
  variables:
    # References for the build script
    CURRENT_VCS_REF_NAME: "${CI_COMMIT_REF_NAME}"
    BUILT_IMAGE_NAME: "${RUNTIME_CANONICAL_IMAGE_NAME}:${CI_COMMIT_REF_NAME}-latest"
  before_script:
    - buildah login --username "${CI_REGISTRY_USER}" --password "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
  script:
    - ./containers/build_buildah_runtime.sh
    - buildah tag "${BUILT_IMAGE_NAME}" "${RUNTIME_IMAGE_NAME}"
    - buildah push "${RUNTIME_IMAGE_NAME}"
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: always
    - if: $CI_PIPELINE_SOURCE == "web"
      when: always
    - if: $CI_PIPELINE_SOURCE == "trigger"
      when: always
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - changes:
        - pyproject.toml
        - containers/build_buildah_runtime.sh
      when: always
    - !reference [.do-not-run-on-po-change, rules]
    # It is possible to trigger it manually through the web interface, but
    # we allow_failure to prevent blocking the other jobs
    - when: manual
      allow_failure: true


##############################################################################
###################               Build                    ###################
##############################################################################
pages:
  stage: build
  extends: .job-on-runtime-image
  script:
    - pip install ".[docs]"
    - cd docs && ./build.sh && cd ..
  after_script:
    - mv docs/build/ _docs/
  artifacts:
    name: "${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}"
    paths:
      - _docs
    expire_in: 1 week
  rules:
    - !reference [.do-not-run-on-po-change, rules]
    - changes:
        - docs/*
      when: always
    # Run the job on the default branch only.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
    - when: never


package:
  stage: build
  extends: .job-on-runtime-image
  script:
    - pip install build
    - python -m build
  artifacts:
    name: "${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}"
    paths:
      - dist/
    expire_in: 1 week
  rules:
    - !reference [.do-not-run-on-po-change, rules]
    - when: always



build_container:production:
  extends: .build_damned_lies_image
  stage: build
  rules:
    - !reference [ .do-not-run-on-po-change, rules ]
    - !reference [.do-not-run-if-not-on-main-branch, rules]
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
    - when: never


build_container:test:
  extends: .build_damned_lies_image
  stage: build
  variables:
    KIND_OF_DEPLOYMENT: "test"
  rules:
    - !reference [.do-not-run-on-po-change, rules]
    - if: $CI_COMMIT_BRANCH == "develop"
      when: always
    - when: never



##############################################################################
###################               Test                     ###################
##############################################################################
test:
  stage: test
  extends: .job-on-runtime-image
  dependencies: []
  allow_failure: false
  script:
    - pip install ".[dev]"
    - python manage.py compile-trans --settings=damnedlies.settings_tests
    # In the container, /tmp is not tmpfs and some error occur with the @test_strachdir decorator
    - git config --global --add safe.directory "*"
    - coverage run --concurrency=multiprocessing manage.py test --settings=damnedlies.settings_tests -v3 --parallel
  after_script:
    - coverage combine
    - coverage xml
    - coverage report | tee coverage.txt
  coverage: /^TOTAL.*\s+(\d+\%)$/
  artifacts:
    name: "${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}"
    expire_in: 1 week
    expose_as: "Unit tests coverage"
    paths:
      - coverage.txt
      - tests-report.xml
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      junit: tests-report.xml
  rules:
    - !reference [.do-not-run-on-po-change, rules]
    - when: always

.static-analysis:
  stage: test
  dependencies: []
  allow_failure: false
  rules:
    - !reference [.do-not-run-on-po-change, rules]
    - when: always

static-analysis:pre-commit:
  extends:
    - .static-analysis
    - .job-on-runtime-image
  script:
    - pip install ".[dev]"
    - python manage.py compile-trans --settings=damnedlies.settings_tests
    - git config --global --add safe.directory "*"
    - pre-commit run --all-files

static-analysis:pylint:
  extends:
    - .static-analysis
    - .job-on-runtime-image
  allow_failure: false
  script:
    - pip install ".[dev]"
    - pylint . | tee pylint.txt
  artifacts:
    name: "pylint-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}"
    expire_in: 1 week
    expose_as: "Pylint Analysis - all files"
    paths:
      - pylint.txt
    when: always


static-analysis:ruff-all:
  extends:
    - .static-analysis
    - .job-on-runtime-image
  allow_failure: true
  script:
    - pip install ".[dev]"
    - ruff check | tee ruff.txt
  artifacts:
    name: "ruff-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}"
    expire_in: 1 week
    expose_as: "Ruff Analysis - all files"
    paths:
      - ruff.txt
    when: always


static-analysis:ruff-no-fail:
  extends:
    - .static-analysis
    - .job-on-runtime-image
  script:
    - pip install ".[dev]"
    - ruff check api languages feeds people teams | tee ruff-no-fail.txt
  artifacts:
    name: "ruff-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}"
    expire_in: 1 week
    expose_as: "Ruff Analysis"
    paths:
      - ruff-no-fail.txt
    when: always

#############################################################################
###################               Deploy                  ###################
#############################################################################

.deploy:
  extends: .job-on-buildah
  before_script:
    - buildah login --username "${OCI_REGISTRY_USER}" --password "${OCI_REGISTRY_PASSWORD}" "${OCI_REGISTRY}"
  script:
    - IMAGE_ID="$(buildah pull -q oci-archive:damned-lies-production_$CI_COMMIT_REF_NAME.tar)"
    - buildah tag "${IMAGE_ID}" "${OCI_REGISTRY}/gnome_infrastructure/damned-lies:${IMAGE_TAG_ON_REGISTRY}"
    - buildah push "${OCI_REGISTRY}/gnome_infrastructure/damned-lies:${IMAGE_TAG_ON_REGISTRY}"


deploy:staging:
  extends: .deploy
  stage: staging
  variables:
    IMAGE_TAG_ON_REGISTRY: "staging"
  dependencies:
    - build_container:production
  environment:
    name: l10n-staging.gnome.org
    url: https://damned-lies-staging.apps.openshift4.gnome.org
    action: start
    deployment_tier: staging
  rules:
    - !reference [.do-not-run-on-merge-request, rules]
    - !reference [.do-not-run-on-po-change, rules]
    - !reference [.do-not-run-if-not-on-main-branch, rules]
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: on_success
    - when: never


deploy:production:
  extends: .deploy
  stage: deploy
  variables:
    IMAGE_TAG_ON_REGISTRY: "latest"
  dependencies:
    - build_container:production
  environment:
    name: l10n.gnome.org
    url: https://l10n.gnome.org
    action: start
    deployment_tier: production
  rules:
    - !reference [.do-not-run-on-merge-request, rules]
    - !reference [.do-not-run-on-po-change, rules]
    - !reference [.do-not-run-if-not-on-main-branch, rules]
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
    - when: never


deploy:test:
  extends: .deploy
  stage: deploy
  variables:
    IMAGE_TAG_ON_REGISTRY: "testing"
  dependencies:
    - build_container:test
  environment:
    name: l10n-testing.gnome.org
    url: https://damned-lies-testing.apps.openshift4.gnome.org
    action: start
    deployment_tier: testing
  rules:
    - !reference [.do-not-run-on-merge-request, rules]
    - !reference [.do-not-run-on-po-change, rules]
    - if: $CI_COMMIT_BRANCH == "develop"
      when: on_success
    - when: never

pages:deploy:
  stage: deploy
  needs: ['pages']
  script:
    - mv _docs public
  artifacts:
    paths:
      - public
  rules:
    - !reference [.do-not-run-on-merge-request, rules]
    - !reference [.do-not-run-on-po-change, rules]
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: on_success
