**Please read this!**

Before opening a new issue, make sure to search for keywords in the issues filtered by the ~"Bug" label, and check the issue you’re about to submit isn’t a duplicate. Remove this notice if you’re confident your issue isn’t a duplicate.

Please take time to fill in the section that are relevant to your situation. If a section is not relevant for your special case, feel free to remove it

------

### Summary

(summarize the bug encountered concisely)

### Steps to reproduce
(how one can reproduce the issue. Give details about your configuration (such as the browser you are using).
The idea here is to let the developers reproduce the bug on their own systems. If you have installed Damned Lies manually on your system, provide your Python version number (` python3 --version`) and the list of your modules (`pip list`) − remember to display them inside a block markup (```)).

### What is the current *bug* behavior?

(what actually happens)

### What is the expected *correct* behavior?

(what you should see instead)

### Relevant logs and/or screenshots

(paste any relevant log − please use code blocks (```) to format console output, error messages), provide screenshots if they can help better understanding the issue).

### Possible fixes

(if you can, get the link to the line of code that might be responsible for the problem. Anyway, if you can propose a solution, no necessarily a code fix but any information that is the beginning of a fix)

/label ~"Bug"
