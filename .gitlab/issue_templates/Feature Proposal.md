**Please read this!**

Before opening a new issue, make sure to search for keywords in the issues filtered by the ~"Feature request" label, and check the issue you’re about to submit isn’t a duplicate. Remove this notice if you’re confident your issue isn’t a duplicate.

Please take time to fill in the section that are relevant to your situation. If a section is not relevant for your special case, feel free to remove it. There are numerous sections in this document you may not fill by yourself, we will use them in order to synthesize discussion, and the challenges of the feature request.

------
### Description

(include the issue you encountered, use cases, benefits, and/or goals)

### Proposal

(describe what you propose in order to fix the issue you encountered, the feature you want to be added in the software)

#### Potential issues

(list here all the problems related to the feature, when it will be implemented. Try to be as exhaustive as you wish/can, as this will be used to determine whether the feature should be implemented.)

### Links / references

(a list of links and/or references that help understand the issue, try to give solutions, clues, etc.)

### Documentation blurb

(write the start of the documentation of this feature here, include:

1. Why should someone use it; what’s the underlying problem.
2. What is the solution.
3. How does someone use this)

### List of tests

(a list of the tests you think will be able to validate the code that fixes the issue. This is written in natural language and may fix the potential issues you raised just before. Don’t forget to explore the “limits” of the feature.)

/label ~"Feature request"
/label ~"Enhancement"
/label ~"Needs review"
