import abc
import traceback
from abc import ABC
from collections.abc import Iterable
from threading import Thread
from typing import TYPE_CHECKING, Any

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import mail_admins
from django.db.models import QuerySet
from django.http import Http404, HttpRequest, HttpResponseForbidden, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from common.utils import check_gitlab_request
from languages.models import Language
from stats.doap import update_doap_infos
from stats.models import Branch, Module, Release, Statistics
from teams.models import Team
from vertimus.forms import ActionForm
from vertimus.models import ActionRT, ActionUT
from vertimus.views import get_vertimus_state

if TYPE_CHECKING:
    from vertimus.models import State


class SerializeListView(View, ABC):
    fields = tuple()

    @abc.abstractmethod
    def get_href(self: "SerializeListView", serialized_instance: dict[str, Any]) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def get_queryset(self: "SerializeListView") -> QuerySet:
        raise NotImplementedError()

    def get(self: "SerializeListView", *args, **kwargs) -> JsonResponse:  # noqa: ANN002, ANN003, ARG002
        return JsonResponse(self.__serialize_queryset(self.get_queryset()), safe=False)

    def __serialize_queryset(self: "SerializeListView", queryset: "QuerySet") -> list[dict[str, str]]:
        """
        Serialize a full QuerySet by calling serialize_object() of each of its item.
        """
        return [
            self.__add_href_to_serialized_object(object_as_dict)
            # We extract the values of the objects in the queryset to keep only
            # some of the fields.
            for object_as_dict in queryset.values(*self.fields)
        ]

    def __add_href_to_serialized_object(self: "SerializeListView", object_as_dict: dict[str, Any]) -> dict[str, str]:
        """
        Adds a ‘href’ key in order to link each element to its fully qualified URL.
        """
        object_as_dict["href"] = self.get_href(object_as_dict)
        return object_as_dict


class SerializeObjectView(View, ABC):
    fields = tuple()

    @abc.abstractmethod
    def get_object(self: "SerializeObjectView") -> object:
        raise NotImplementedError()

    def get(self: "SerializeObjectView", *args, **kwargs) -> JsonResponse:  # noqa: ARG002, ANN002, ANN003
        return JsonResponse(self.serialize_object(self.get_object()))

    @abc.abstractmethod
    def serialize_object(self: "SerializeObjectView", object_to_serialize: object) -> dict[str, Any]:
        return self._serialize_instance(object_to_serialize, self.fields)

    @staticmethod
    def _serialize_instance(instance: object, fields: Iterable[str]) -> dict[str, Any]:
        serialized_instance = {}
        for field in fields:
            value = getattr(instance, field)
            # The object is a QuerySet, so all the elements should be retrieved.
            if hasattr(value, "all"):
                value = [str(o) for o in value.all()]
            serialized_instance[field] = value
        return serialized_instance


# noinspection PyMethodMayBeStatic
class APIHomeView(View):
    """
    List available API endpoints.
    """

    def get(self: "APIHomeView", *args, **kwargs) -> JsonResponse:  # noqa: PLR6301, ARG002, ANN002, ANN003
        return JsonResponse({
            "modules": reverse("api-v1-list-modules"),
            "teams": reverse("api-v1-list-teams"),
            "languages": reverse("api-v1-list-languages"),
            "releases": reverse("api-v1-list-releases"),
        })


# noinspection PyMethodMayBeStatic
class ModulesView(SerializeListView):
    """
    List all available and active modules.
    """

    fields = ("name",)

    def get_queryset(self: "ModulesView") -> QuerySet[Module]:  # noqa: PLR6301
        return Module.objects.filter(archived=False)

    def get_href(self: "ModulesView", serialized_module: dict[str, Any]) -> str:  # noqa: PLR6301
        return reverse("api-v1-info-module", kwargs={"module_name": serialized_module.get("name")})


class ModuleView(SerializeObjectView):
    @property
    def fields(self: "ModuleView") -> list[str]:
        """
        List all the fields of a module, excluding the ones involved in relation to other models and
        private attributes.
        """
        return [str(field.name) for field in Module._meta.get_fields() if not field.one_to_many and field.name != "id"]

    def get_object(self: "ModuleView") -> Module:
        return get_object_or_404(Module, name=self.kwargs["module_name"])

    def serialize_object(self: "ModuleView", object_to_serialize: Module) -> dict[str, list | Any]:
        serialized_module = self._serialize_instance(object_to_serialize, self.fields)
        serialized_module["branches"] = [
            self._serialize_instance(branch, ["name"]) for branch in object_to_serialize.get_branches()
        ]
        serialized_module["domains"] = [
            self._serialize_instance(domain, ["name", "description", "dtype", "layout"])
            for domain in object_to_serialize.domain_set.all().order_by("name")
        ]
        return serialized_module


class ModuleBranchView(SerializeObjectView):
    def get_object(self: "ModuleBranchView") -> Branch:
        return get_object_or_404(
            Branch.objects.select_related("module"),
            module__name=self.kwargs["module_name"],
            name=self.kwargs["branch_name"],
        )

    def serialize_object(self: "ModuleBranchView", object_to_serialize: "Branch") -> dict[str, Any]:  # noqa: PLR6301
        serialized_branch = {
            "module": object_to_serialize.module.name,
            "branch": object_to_serialize.name,
            "domains": [],
        }
        for domain_name in object_to_serialize.connected_domains.keys():
            serialized_statistics = {}
            statistics_for_domain_on_branch_with_language = Statistics.objects.filter(
                branch=object_to_serialize, domain__name=domain_name, language__isnull=False
            )
            for statistic in statistics_for_domain_on_branch_with_language:
                serialized_statistics[statistic.language.locale] = {
                    "trans": statistic.translated(),
                    "fuzzy": statistic.fuzzy(),
                    "untrans": statistic.untranslated(),
                }
            serialized_branch["domains"].append({"name": domain_name, "statistics": serialized_statistics})
        return serialized_branch


# noinspection PyMethodMayBeStatic
class TeamsView(SerializeListView):
    fields = ("name", "description")

    def get_queryset(self: "TeamsView") -> QuerySet:  # noqa: PLR6301
        return Team.objects.all().order_by("name")

    def get_href(self: "TeamsView", serialized_team: dict[str, Any]) -> str:  # noqa: PLR6301
        return reverse("api-v1-info-team", kwargs={"team_name": serialized_team.get("name")})


class TeamView(SerializeObjectView):
    @property
    def fields(self: "TeamView") -> list[str]:
        """
        List all the fields that should be displayed about a team, without many-to-many relations and team members.
        """
        return [str(f.name) for f in Team._meta.get_fields() if not f.one_to_many and f.name not in {"id", "members"}]

    def get_object(self: "TeamView") -> Team:
        return get_object_or_404(Team, name=self.kwargs["team_name"])

    def serialize_object(self: "TeamView", object_to_serialize: Team) -> dict[str, Any]:
        data = self._serialize_instance(object_to_serialize, self.fields)
        data["coordinators"] = [
            self._serialize_instance(coord, ["name"]) for coord in object_to_serialize.get_coordinators()
        ]
        return data


# noinspection PyMethodMayBeStatic
class LanguagesView(SerializeListView):
    fields = (
        "name",
        "locale",
        "team__description",
        "plurals",
    )

    def get_queryset(self: "LanguagesView") -> QuerySet[Language]:  # noqa: PLR6301
        return Language.objects.all().order_by("name")

    def get_href(self: "LanguagesView", serialized_language: dict[str, Any]) -> str:  # noqa: PLR6301
        return reverse("api-v1-info-team", kwargs={"team_name": serialized_language.get("locale")})


# noinspection PyMethodMayBeStatic
class ReleasesView(SerializeListView):
    fields = ("name", "description")

    def get_queryset(self: "ReleasesView") -> Release:  # noqa: PLR6301
        return Release.objects.all().order_by("name")

    def get_href(self: "ReleasesView", serialized_release: dict[str, Any]) -> str:  # noqa: PLR6301
        return reverse("api-v1-info-release", kwargs={"release": serialized_release.get("name")})


class ReleaseView(SerializeObjectView):
    fields = ("name", "description", "string_frozen", "status", "branches")

    def get_object(self: "ReleaseView") -> Release:
        return get_object_or_404(Release, name=self.kwargs["release"])

    def serialize_object(self: "ReleaseView", object_to_serialize: Release) -> dict[str, Any]:
        serialized_release = self._serialize_instance(object_to_serialize, self.fields)
        serialized_release["languages"] = [
            {
                "locale": language.locale,
                "href": reverse(
                    "api-v1-info-release-language",
                    kwargs={"release": object_to_serialize.name, "locale": language.locale},
                ),
            }
            for language in Language.objects.all()
        ]
        serialized_release["statistics"] = reverse(
            "api-v1-info-release-stats", kwargs={"release": self.kwargs.get("release")}
        )
        return serialized_release


class ReleaseStatsView(SerializeObjectView):
    fields = ("name", "description", "status")

    def serialize_object(self: "ReleaseStatsView", object_to_serialize: Release) -> dict[str, Any]:
        lang_stats = object_to_serialize.get_global_stats()
        return {
            "release": self._serialize_instance(object_to_serialize, self.fields),
            "statistics": lang_stats,
        }

    def get_object(self: "ReleaseStatsView") -> Release:
        return get_object_or_404(Release, name=self.kwargs["release"])


class ReleaseLanguageView(View):
    def get(self: "ReleaseLanguageView", *args, **kwargs) -> JsonResponse:  # noqa: ARG002, ANN002, ANN003
        release = get_object_or_404(Release, name=self.kwargs["release"])
        language = get_object_or_404(Language, locale=self.kwargs["locale"])
        item_list = []
        for branch in release.branches.all().select_related("module"):
            for domain in branch.connected_domains:
                item_list.append({
                    "module": branch.module.name,
                    "branch": branch.name,
                    "stats": reverse(
                        "api-v1-module-in-lang-statistics",
                        kwargs={
                            "module_name": branch.module.name,
                            "branch_name": branch.name,
                            "domain_name": domain,
                            "lang": language.locale,
                        },
                    ),
                })
        return JsonResponse({
            "release": release.name,
            "language": language.locale,
            "modules": item_list,
        })


class VertimusPageMixin(View):
    """
    Mixin to be used in conjunction with a View. This is the default of any
    API view to manage the Vertimus workflow.
    """

    def __init__(self: "VertimusPageMixin", **kwargs) -> None:  # noqa: ANN003
        super().__init__(**kwargs)

    def get_state_from_kwargs(self: "VertimusPageMixin") -> tuple[Statistics, "State"]:
        """
        :raises Http404: when there is no domain connected to this module’s branch.
        """
        module = get_object_or_404(Module, name=self.kwargs["module_name"])
        branch = get_object_or_404(module.branch_set, name=self.kwargs["branch_name"])
        domain = branch.connected_domains.get(self.kwargs["domain_name"])
        if not domain:
            raise Http404(
                f"No connected domain ‘{self.kwargs.get('domain_name')}’ found for "
                f"module {module.name} and branch {branch.name}."
            )
        language = get_object_or_404(Language, locale=self.kwargs["lang"])
        return get_vertimus_state(branch, domain, language)[1:]


class ModuleLangStatsView(VertimusPageMixin):
    def get(self: "ModuleLangStatsView", *args, **kwargs) -> JsonResponse:  # noqa: ARG002, ANN002, ANN003
        stats, state = self.get_state_from_kwargs()
        return JsonResponse({
            "module": state.branch.module.name,
            "branch": state.branch.name,
            "domain": state.domain.name,
            "language": state.language.locale,
            "state": state.name,
            "statistics": {"trans": stats.translated(), "fuzzy": stats.fuzzy(), "untrans": stats.untranslated()},
            "pot_file": stats.pot_url(),
            "po_file": stats.po_url(),
        })


@method_decorator(csrf_exempt, name="dispatch")
class ReserveTranslationView(LoginRequiredMixin, VertimusPageMixin):
    def post(
        self: "ReserveTranslationView",
        request: "HttpRequest",
        *args,  # noqa: ARG002, ANN002
        **kwargs,  # noqa: ARG002, ANN003
    ) -> JsonResponse | HttpResponseForbidden:
        _, state = self.get_state_from_kwargs()
        actions = [x.name for x in state.get_available_actions(request.user.person)]
        if "RT" not in actions:
            return HttpResponseForbidden("This module cannot be reserved")

        action = ActionRT(person=request.user.person)
        action.apply_on(state, {"comment": "", "send_to_ml": True})
        return JsonResponse({"result": "OK"})


@method_decorator(csrf_exempt, name="dispatch")
class UploadTranslationView(LoginRequiredMixin, VertimusPageMixin):
    def post(
        self: "UploadTranslationView",
        request: "HttpRequest",
        *args,  # noqa: ARG002, ANN002
        **kwargs,  # noqa: ARG002, ANN003
    ) -> JsonResponse | HttpResponseForbidden:
        _, state = self.get_state_from_kwargs()
        actions = state.get_available_actions(request.user.person)
        if "UT" not in [x.name for x in actions]:
            return HttpResponseForbidden(
                "You cannot upload a translation to this module. This action is not available for the moment."
            )

        action_form = ActionForm(
            request.user,
            state,
            actions,
            data={
                "action": "UT",
                "comment": request.POST.get("comment", ""),
            },
            files=request.FILES,
        )
        if action_form.is_valid():
            action = ActionUT(person=request.user.person, file=action_form.cleaned_data["file"])
            action.apply_on(state, {"comment": action_form.cleaned_data["comment"], "send_to_ml": True})
            return JsonResponse({"result": "OK"})
        return JsonResponse({"result": "Error", "error": action_form.errors})


@csrf_exempt
def refresh_module_branch(
    request: "HttpRequest", module_name: str, branch_name: str
) -> JsonResponse | HttpResponseForbidden:
    """
    Refresh the statistics of a given module branch. This is intended to be
    called externally when the branch of this module has been updated, for
    instance a new commit has been added.

    .. note:: CSRF is skipped, verification using a secret token given in
        ``HTTP_AUTHENTICATION``

    :param request: Django request.
    :param module_name: the module name, ie ``gnome-hello``
    :param branch_name: the module branch, ie ``main``, ``trunk``
    """
    verified = check_gitlab_request(request)
    if not verified:
        return HttpResponseForbidden()

    branch = get_object_or_404(Branch, module__name=module_name, name=branch_name)
    # Run effective work in a separate thread to prevent timeouts
    thread = Thread(target=_update_branch_statistics, args=(branch,))
    thread.start()
    return JsonResponse({"result": "OK"})


def _update_branch_statistics(branch: "Branch") -> None:
    try:
        branch.update_statistics(force=False)
        update_doap_infos(branch.module)
    except Exception:
        traceback_text = traceback.format_exc()
        mail_admins(f"Error while updating {branch.module.name} {branch.name}: {traceback_text}")


def check(_: HttpRequest) -> JsonResponse:
    json_response_data: dict = {"status": "OK"}
    if hasattr(settings, "VERSION_LONG_HASH"):
        json_response_data["version"] = settings.VERSION_LONG_HASH
    return JsonResponse(status=200, data=json_response_data)
