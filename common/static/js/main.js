// ***
// This function shows or hides all modules in a release that are
// 100% translated
// ***
dl = (function($, undefined){
    var COOKIE = "DL_HIDE_COMPLETED_MODULES",
        selector = '.completed-module';

    hide = function() {
        $(selector).hide();
        Cookies.set(COOKIE, 'true');
        $('#show-completed-modules').show();
        $('#hide-completed-modules').hide();
        return false;
    }

    show = function() {
        $(selector).show();
        Cookies.set(COOKIE, 'false');
        $('#show-completed-modules').hide();
        $('#hide-completed-modules').show();
        return false;
    }

    $(function() {
        if (Cookies.get(COOKIE) == "true")
            hide();
        $('#show-completed-modules').click(show);
        $('#hide-completed-modules').click(hide);
    });
})($);
