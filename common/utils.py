import os
import socket
from subprocess import run
from typing import TYPE_CHECKING

from django.conf import settings
from django.core.mail import EmailMessage
from django.utils.translation import get_language
from django.utils.translation import gettext as _

from damnedlies import logger

if TYPE_CHECKING:
    pass

try:
    import icu

    pyicu_present = True
except ImportError:
    pyicu_present = False

MIME_TYPES = {"json": "application/json", "xml": "text/xml"}
STATUS_OK = 0


class CommandLineError(OSError):
    """
    When a command ran on the OS fails.
    """


def run_shell_command(cmd, input_data=None, raise_on_error=False, env=None, cwd=None, **extra_kwargs):
    logger.debug(cmd)

    if env is not None:
        env = dict(os.environ, **env)

    shell = not isinstance(cmd, list)
    result = run(
        cmd,
        shell=shell,
        input=input_data,
        encoding="utf-8",
        capture_output=True,
        env=env,
        cwd=cwd,
        **extra_kwargs,
    )
    status = result.returncode
    logger.debug(result.stdout + result.stderr)
    if raise_on_error and status != STATUS_OK:
        raise CommandLineError(status, f"Command: ‘{cmd}’, Error: {result.stderr or result.stdout}")

    return status, result.stdout, result.stderr


def lc_sorted(*args, **kwargs):
    """
    Same as the built-in function ``sorted`` but according to the current locale.
    """
    if pyicu_present:
        collator = icu.Collator.createInstance(icu.Locale(str(get_language())))
        key = kwargs.get("key", lambda x: x)
        kwargs["key"] = lambda x: collator.getSortKey(key(x))
    return sorted(*args, **kwargs)


def trans_sort_object_list(lst, tr_field):
    """Sort an object list with translated_name"""
    for item in lst:
        item.translated_name = _(getattr(item, tr_field))
    return lc_sorted(lst, key=lambda o: o.translated_name.lower())


def is_site_admin(user):
    return user.is_superuser or settings.ADMIN_GROUP in [g.name for g in user.groups.all()]


def send_mail(subject, message, **kwargs):
    """Wrapper to Django's send_mail allowing all EmailMessage init arguments."""
    if not subject.startswith(settings.EMAIL_SUBJECT_PREFIX):
        subject = f"{settings.EMAIL_SUBJECT_PREFIX}{subject}"
    EmailMessage(subject, message, **kwargs).send()


def check_gitlab_request(request):
    remote_ip = request.META.get("HTTP_X_FORWARDED_FOR", request.META.get("REMOTE_ADDR"))
    if "," in remote_ip:
        remote_ip = remote_ip.split(",")[0].strip()
    try:
        from_host = socket.gethostbyaddr(remote_ip)[0]
    except socket.herror:
        return False
    except Exception as exc:
        raise Exception("Unable to get host for address '%s'" % remote_ip) from exc
    return request.method == "POST" and (
        from_host == "gitlab.gnome.org"
        or (
            request.META.get("HTTP_X_GITLAB_EVENT") == "Push Hook"
            and request.META.get("HTTP_X_GITLAB_TOKEN") == settings.GITLAB_TOKEN
        )
    )
