#!/bin/bash

# KIND_OF_DEPLOYMENT: kind of image to build (the configuration will depend on this parameter).
# Possible values are test, production (the default), the names of the configuration files in JSON
declare KIND_OF_DEPLOYMENT="${1:-"production"}"

CONTAINER_NAME="damned-lies-${KIND_OF_DEPLOYMENT}"
declare -r CONTAINER_NAME

VOLUME_NAME="damned-lies-data-${KIND_OF_DEPLOYMENT}"
declare -r VOLUME_NAME

USER_ID=10100
declare -r USER_ID

podman rm "${CONTAINER_NAME}" -f

podman volume rm "${VOLUME_NAME}" -f
podman volume create "${VOLUME_NAME}" --opt o=uid="${USER_ID}",gid="${USER_ID}"

mkdir -p logs
chmod ugo+rwx logs

podman run \
        -d --rm \
        --name="${CONTAINER_NAME}" \
        --publish 8080:8080/tcp \
        --env SECRET_KEY="random_string" \
        --volume "${VOLUME_NAME}:/var/www/djamnedlies/data":Z \
        --volume "./logs:/var/www/djamnedlies/data/logs":Z \
        --user ${USER_ID}:${USER_ID} \
        damned-lies-production:master-"${KIND_OF_DEPLOYMENT}"
