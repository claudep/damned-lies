import logging
import tempfile

from .settings import *


DEBUG = True
DEBUG_TEMPLATES = False
USE_DEBUG_TOOLBAR = False

SECRET_KEY = "shRc(?sk+sW3Wqn-lBvs=r52a@#hgC9g"

SCRATCHDIR = BASE_DIR.parent / "scratch"
POT_DIR = SCRATCHDIR / "POT"
GETTEXT_ITS_DATA = {}

LOCK_DIR = Path(tempfile.mkdtemp())

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

try:
    import xmlrunner

    TEST_RUNNER = "xmlrunner.extra.djangotestrunner.XMLTestRunner"
    TEST_OUTPUT_DIR = open("tests-report.xml", "wb")
except ImportError:
    pass

# In case the developer would like to update these settings, it’s possible
# to add some in a local_settings module
try:
    from .local_settings import *
except ImportError:
    pass

logger = logging.getLogger("damnedlies")
logger.setLevel(logging.DEBUG)

if DEBUG and DEBUG_TEMPLATES:
    TEMPLATES[0]["OPTIONS"]["string_if_invalid"] = (
        "Missing variable: %s" if DEBUG else ""
    )
