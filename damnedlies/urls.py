from django.conf import settings
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, path, re_path, register_converter
from django.urls.converters import StringConverter
from django.views.decorators.http import require_POST
from django.views.generic import TemplateView
from django.views.static import serve

from common import views as common_views
from stats import views as stats_views


class LocaleConverter(StringConverter):
    regex = "[-_a-zA-Z@]+"


class NameConverter(StringConverter):
    """Converter for module, branch, or domain names."""

    regex = r"[-~\w\+\.]+"


register_converter(LocaleConverter, "locale")
register_converter(NameConverter, "name")
module_branch_domain = "<name:module_name>/<path:branch_name>/<name:domain_name>"

urlpatterns = [
    path("", common_views.index, name="home"),
    path("about/", common_views.about, name="about"),
    path("login/", common_views.LoginView.as_view(template_name="login.html"), name="login"),
    path("logout/", require_POST(auth_views.LogoutView.as_view()), name="logout"),
    path("register/", common_views.site_register, name="register"),
    re_path(r"^help/(?P<topic>\w+)/(?P<modal>[0-1])?/?$", common_views.help_view, name="help"),
    path(
        "register/success/",
        TemplateView.as_view(template_name="registration/register_success.html"),
        name="register_success",
    ),
    path("register/activate/<slug:key>", common_views.activate_account, name="register_activation"),
    path(
        "password_reset/",
        auth_views.PasswordResetView.as_view(template_name="registration/password_reset_form.html"),
        name="password_reset",
    ),
    path(
        "password_reset/done/",
        auth_views.PasswordResetDoneView.as_view(template_name="registration/password_reset_done.html"),
        name="password_reset_done",
    ),
    path(
        "reset/<slug:uidb64>/<slug:token>/",
        auth_views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path("reset/done/", auth_views.PasswordResetCompleteView.as_view(), name="password_reset_complete"),
    # Webhook endpoint
    path("pull_code/", common_views.pull_code),
    path("teams/", include("teams.urls")),
    path("people/", include("people.urls")),
    # users is the hardcoded url in the contrib.auth User class, making it identical to /people
    path("users/", include("people.urls")),
    path("languages/", include("languages.urls")),
    path("vertimus/", include("vertimus.urls")),
    path("i18n/", include("django.conf.urls.i18n")),
    path("admin/", admin.site.urls),
    path("rss/", include("feeds.urls")),
    path("api/v1/", include("api.urls")),
]

urlpatterns += [
    re_path(r"^module/(?P<return_format>(html|json|xml))?/?$", stats_views.modules, name="modules"),
    path("module/po/%s/<filename>" % module_branch_domain, stats_views.dynamic_po, name="dynamic_po"),
    path("module/<name:module_name>/", stats_views.module, name="module"),
    path("module/<name:module_name>/edit/branches/", stats_views.module_edit_branches, name="module_edit_branches"),
    path("branch/<int:branch_id>/refresh/", stats_views.branch_refresh, name="branch_refresh"),
    path(
        "module/<name:module_name>/<name:potbase>/<path:branch_name>/<locale:langcode>/images/",
        stats_views.docimages,
        name="docimages",
    ),
    re_path(r"^releases/(?P<format>(html|json|xml))?/?$", stats_views.releases, name="releases"),
    re_path(r"^releases/(?P<release_name>[\w-]+)/(?P<format>(html|xml))?/?$", stats_views.release, name="release"),
    path("releases/compare/<dtype>/<path:rels_to_compare>/", stats_views.compare_by_releases, name="release-compare"),
    # Ajax URLs
    path("module/<name:module_name>/branch/<path:branch_name>/", stats_views.ajax_module_branch, name="module_branch"),
]

if settings.USE_DEBUG_TOOLBAR:
    import debug_toolbar

    urlpatterns += [
        path("__debug__/", include(debug_toolbar.urls)),
    ]

if settings.STATIC_SERVE:
    urlpatterns += [
        path("media/<path:path>", serve, kwargs={"document_root": settings.MEDIA_ROOT}),
        path("POT/<path:path>", serve, kwargs={"document_root": settings.POT_DIR}),
        path("HTML/<path:path>", serve, kwargs={"document_root": settings.SCRATCHDIR / "HTML"}),
    ]
