``damned-lies``
===============

``damned-lies`` is a `Django <https://djangoproject.com>`_ application and is part of the `GNOME <https://www.gnome.org>`_ project. It aims at providing an easy way to manage module translations. With Damned Lies, you can translate modules (*ie* ``gnome-hello``) with specific branches (*ie* ``develop``, ``main``…), and domains, if you provide multiple translatable components (*ie* documentation, user-interface, Windows installer, etc.)

.. warning::
   This documentation is updated against the project main branch only. Please go the `Damned Lies project page <https://gitlab.gnome.org/Infrastructure/damned-lies>`_ in order to see what if refers to.

API documentation
-----------------

.. toctree::
   :maxdepth: 2

   modules
   restapi

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
