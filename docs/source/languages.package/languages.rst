languages package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   languages.management

Submodules
----------

languages.admin module
----------------------

.. automodule:: languages.admin
   :members:
   :private-members:
   :show-inheritance:

languages.models module
-----------------------

.. automodule:: languages.models
   :members:
   :private-members:
   :show-inheritance:

languages.views module
----------------------

.. automodule:: languages.views
   :members:
   :private-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: languages
   :members:
   :private-members:
   :show-inheritance:
