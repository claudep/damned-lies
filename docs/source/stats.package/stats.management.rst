stats.management package
========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

Module contents
---------------

.. automodule:: stats.management
   :members:
   :private-members:
   :show-inheritance:
