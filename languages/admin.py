from django.contrib import admin

from languages.models import Language


class LanguageAdmin(admin.ModelAdmin):
    search_fields = ("name", "locale")
    list_display = ("name", "locale", "team", "plurals")
    list_editable = ("locale", "team", "plurals")


admin.site.register(Language, LanguageAdmin)
