import hashlib
from secrets import token_bytes
from typing import Any

import requests
from django import forms
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.exceptions import ValidationError
from django.http import HttpRequest
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy
from PIL import ImageFile
from requests import HTTPError
from requests.exceptions import ConnectionError as RequestConnectionError
from requests.exceptions import InvalidSchema, MissingSchema

from common.utils import send_mail
from people.models import Person
from teams.models import Team

username_validator = UnicodeUsernameValidator()


class RegistrationForm(forms.Form):
    """Form for user registration"""

    username = UsernameField(
        max_length=30,
        label=gettext_lazy("Choose a username:"),
        validators=[username_validator],
        help_text=gettext_lazy("May contain only letters, numbers, and @/./+/-/_ characters."),
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    email = forms.EmailField(widget=forms.TextInput(attrs={"class": "form-control"}), label=gettext_lazy("Email:"))
    password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control"}, render_value=False),
        label=gettext_lazy("Password:"),
        required=False,
        min_length=7,
        help_text=gettext_lazy("At least 7 characters"),
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control"}, render_value=False),
        label=gettext_lazy("Confirm password:"),
        required=False,
    )

    def clean_username(self: "RegistrationForm") -> str:
        """
        Validate the username

        :raises ValidationError: when the username is already taken by another user.
        """
        try:
            Person.objects.get(username__iexact=self.cleaned_data["username"])
        except Person.DoesNotExist:
            return self.cleaned_data["username"]
        raise ValidationError(_("This username is already taken. Please choose another."))

    def clean(self: "RegistrationForm") -> dict[str, Any]:
        cleaned_data = self.cleaned_data
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")
        if not password1:
            raise ValidationError(_("You must provide a password"))

        if password1 and password1 != password2:
            raise ValidationError(_("The passwords do not match"))
        return cleaned_data

    def save(self: "RegistrationForm", request: HttpRequest) -> "Person":  # noqa: ARG002
        """Create the user"""
        username = self.cleaned_data["username"]
        email = self.cleaned_data["email"]
        password = self.cleaned_data["password1"]

        salt = hashlib.sha512(force_bytes(token_bytes(32)), usedforsecurity=True).hexdigest()[:5]
        activation_key = hashlib.sha512(force_bytes(salt + username), usedforsecurity=True).hexdigest()

        new_user = Person.objects.create_user(
            username=username, email=email, password=password, activation_key=activation_key, is_active=False
        )
        new_user.save()

        RegistrationForm._send_activation_email(activation_key, email)

        return new_user

    @staticmethod
    def _send_activation_email(activation_key: str, email: str) -> None:
        site_domain = settings.SITE_DOMAIN
        activation_url = str(reverse("register_activation", kwargs={"key": activation_key}))
        message = (
            _(
                "This is a confirmation that your registration on %s succeeded. To activate your account, "
                "please click on the link below or copy and paste it in a browser."
            )
            % site_domain
        )
        message += f"\n\nhttps://{site_domain}{activation_url}\n\n"
        message += _("Administrators of %s") % site_domain
        send_mail(_("Account activation"), message, to=[email])


class LoginForm(AuthenticationForm):
    def clean_username(self: "LoginForm") -> str:
        username = self.cleaned_data["username"]
        if "@" in username and not Person.objects.filter(username=username).exists():
            try:
                username = Person.objects.filter(email=username).first().username
            except AttributeError:
                pass
        return username


class DetailForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = (
            "username",
            "first_name",
            "last_name",
            "email",
            "image",
            "avatar_service",
            "webpage_url",
            "im_username",
            "forge_account",
            "forum_account",
        )

    def clean_forge_account(self: "DetailForm") -> str:
        forge_account = self.cleaned_data["forge_account"]
        if forge_account and forge_account.startswith("@"):
            forge_account = forge_account.removeprefix("@")
        return forge_account

    def clean_image(self: "DetailForm") -> str:
        url = self.cleaned_data["image"]
        max_width, max_height = Person.MAX_IMAGE_SIZE
        if url:
            width, height = get_image_size_from_url(url)
            if width > max_width or height > max_height:
                raise ValidationError(
                    _("Image too high or too wide (%(width)d×%(height)d, maximum is 100×100 pixels)")
                    % {"width": width, "height": height}
                )
        return url


class TeamChoiceField(forms.ModelChoiceField):
    def label_from_instance(self: "TeamChoiceField", team: Team) -> str:  # noqa: PLR6301
        return team.get_description()


class TeamJoinForm(forms.Form):
    def __init__(self: "TeamJoinForm", *args: list[Any], **kwargs: dict[str, Any]) -> None:
        super().__init__(*args, **kwargs)
        # FIXME: exclude team to which user is already member
        self.fields["teams"] = TeamChoiceField(queryset=Team.objects.all())


def get_image_size_from_url(url: str) -> tuple[int, int]:
    """
    Returns the width and height (as a tuple) of the image pointed at by the url.

    :raises ValidationError: when there is an error getting the image size

    .. seealso::
        `Pillow ImageFile documentation. <https://pillow.readthedocs.io/en/stable/reference/ImageFile.html>`_

        Code partially copied from http://effbot.org/zone/pil-image-size.htm (dead link)
    """
    try:
        r = requests.get(url, stream=True, timeout=2)
        r.raise_for_status()
    except (HTTPError, RequestConnectionError, MissingSchema, InvalidSchema) as exc:
        raise ValidationError(_("There was an error retrieving the image file (%s)") % exc) from exc
    r.raw.decode_content = True
    image_file = r.raw

    size = None
    image_file_parser = ImageFile.Parser()
    try:
        while True:
            data = image_file.read(1024)
            if not data:
                break
            image_file_parser.feed(data)
            if image_file_parser.image:
                size = image_file_parser.image.size
                break
    except Exception as exc:
        raise ValidationError(_("Sorry, an error occurred while trying to get image size (%s)") % str(exc)) from exc

    if not size:
        raise ValidationError(_("The URL you provided seems not to correspond to a valid image"))

    return size
