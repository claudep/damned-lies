import django.contrib.auth.models
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("auth", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Person",
            fields=[
                (
                    "user_ptr",
                    models.OneToOneField(
                        parent_link=True,
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.CASCADE,
                    ),
                ),
                ("svn_account", models.SlugField(max_length=20, null=True, blank=True)),
                (
                    "image",
                    models.URLField(
                        help_text="URL to an image file (.jpg, .png, …) of an hackergotchi (max. 100×100 pixels)",
                        null=True,
                        verbose_name="Image",
                        blank=True,
                    ),
                ),
                (
                    "use_gravatar",
                    models.BooleanField(default=False, help_text="Display the image of your gravatar.com account"),
                ),
                ("webpage_url", models.URLField(null=True, verbose_name="Web page", blank=True)),
                ("irc_nick", models.SlugField(max_length=20, null=True, verbose_name="IRC nickname", blank=True)),
                (
                    "bugzilla_account",
                    models.EmailField(
                        help_text="This should be an email address, useful if not equal to “E-mail address” field",
                        max_length=254,
                        null=True,
                        verbose_name="Bugzilla account",
                        blank=True,
                    ),
                ),
                ("activation_key", models.CharField(max_length=40, null=True, blank=True)),
            ],
            options={
                "ordering": ("username",),
                "db_table": "person",
            },
            bases=("auth.user",),
            managers=[
                ("objects", django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
