from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("people", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="person",
            name="use_gravatar",
            field=models.BooleanField(
                default=False, help_text="Display the image of your gravatar.com account", verbose_name="Use gravatar"
            ),
        ),
    ]
