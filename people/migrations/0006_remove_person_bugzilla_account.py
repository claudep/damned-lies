from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("people", "0005_remove_person_use_gravatar"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="person",
            name="bugzilla_account",
        ),
    ]
