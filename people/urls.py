from django.contrib.auth.decorators import login_required
from django.urls import path

from people import views

# Path order is really important here
urlpatterns = [
    path("detail_change/", login_required(views.PersonEditView.as_view()), name="person_detail_change"),
    path("password_change/", views.person_password_change, name="person_password_change"),
    path("create_token/", views.person_create_token, name="person_create_token"),
    path("delete_token/", views.person_delete_token, name="person_delete_token"),
    path("team_join/", views.person_team_join, name="person_team_join"),
    path("team_leave/<locale:team_slug>/", views.person_team_leave, name="person_team_leave"),
    path("<int:pk>/", views.PersonDetailView.as_view(), name="person_detail_id"),
    # Equivalent to the previous, but using username instead of user pk
    path("<slug>/", views.PersonDetailView.as_view(), name="person_detail_username"),
    path("", views.PeopleListView.as_view(), name="people"),
]
