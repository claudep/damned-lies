from django import forms
from django.contrib import admin, messages
from django.contrib.admin import helpers
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import render

from stats.doap import update_doap_infos
from stats.models import (
    Branch,
    Category,
    CategoryName,
    Domain,
    Information,
    Module,
    PoFile,
    Release,
    Statistics,
)


class DomainForm(forms.ModelForm):
    class Meta:
        model = Module
        fields = "__all__"

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields["branch_from"].queryset = self.fields["branch_from"].queryset.filter(
                module=self.instance.module
            )
            self.fields["branch_to"].queryset = self.fields["branch_to"].queryset.filter(module=self.instance.module)
        else:
            self.fields["branch_from"].queryset = Branch.objects.none()
            self.fields["branch_to"].queryset = Branch.objects.none()


class BranchInline(admin.TabularInline):
    model = Branch


class DomainInline(admin.StackedInline):
    model = Domain
    form = DomainForm
    fieldsets = (
        (None, {"fields": (("name", "description", "dtype", "layout"),)}),
        (
            "Advanced",
            {
                "fields": (
                    ("pot_method", "pot_params"),
                    "extra_its_dirs",
                    "linguas_location",
                    "red_filter",
                    ("branch_from", "branch_to"),
                ),
                "classes": ("collapse",),
            },
        ),
    )

    def get_formset(self, request, obj=None, **kwargs):
        # Hack! Store parent obj for formfield_for_foreignkey
        self.parent_obj = obj
        return super().get_formset(request, obj, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == "description":
            kwargs["widget"] = forms.Textarea(attrs={"rows": "1", "cols": "20"})
        elif db_field.name in {"name", "layout"}:
            kwargs["widget"] = forms.TextInput(attrs={"size": "20"})
        elif db_field.name in {"red_filter", "extra_its_dirs"}:
            kwargs["widget"] = forms.Textarea(attrs={"rows": "1", "cols": "40"})
        return super().formfield_for_dbfield(db_field, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name in {"branch_from", "branch_to"} and hasattr(self, "parent_obj") and self.parent_obj:
            kwargs["queryset"] = self.parent_obj.branch_set.all()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class ModuleForm(forms.ModelForm):
    class Meta:
        model = Module
        fields = "__all__"

    def save(self, **kwargs):
        must_renew_checkout = "vcs_root" in self.changed_data and not self.instance._state.adding and not self.errors
        if must_renew_checkout:
            old_module = Module.objects.get(pk=self.instance.pk)
            # Delete checkout(s)
            for branch in old_module.get_branches(reverse=True):  # head branch last
                branch.delete_checkout()
        instance = super().save(**kwargs)
        if must_renew_checkout:
            for branch in instance.get_branches():
                # Force checkout and updating stats
                branch.save()
        return instance


class ModuleAdmin(admin.ModelAdmin):
    form = ModuleForm
    fieldsets = (
        (
            None,
            {
                "fields": (
                    ("name", "description"),
                    "homepage",
                    "comment",
                    "archived",
                    ("bugs_base",),
                    ("vcs_root", "vcs_web"),
                    "ext_platform",
                    "maintainers",
                )
            },
        ),
    )
    inlines = (BranchInline, DomainInline)
    search_fields = ("name", "homepage", "comment", "vcs_web")
    list_display = ("__str__", "name", "vcs_root", "ext_platform", "archived")
    list_filter = ("archived",)

    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "description":
            field.widget.attrs["rows"] = "1"
        elif db_field.name == "comment":
            field.widget.attrs["rows"] = "4"

        return field

    def save_related(self, request, form, *args, **kwargs) -> None:
        super().save_related(request, form, *args, **kwargs)
        # Here, branches should have been created.
        update_doap_infos(form.instance)

    def delete_model(self, request, obj: Module) -> None:
        for branch in obj.get_branches():
            branch.delete_checkout()
        obj.delete()


class BranchAdmin(admin.ModelAdmin):
    search_fields = ("name", "module__name")
    list_display = (
        "name",
        "module",
        "weight",
        "checkout_on_creation",
        "uses_meson",
        "is_head",
        "has_string_frozen",
        "is_archive_only",
        "is_vcs_readonly",
        "vcs_web_url",
    )


class DomainAdmin(admin.ModelAdmin):
    form = DomainForm
    list_display = ("__str__", "dtype", "layout", "pot_method")
    list_filter = ("dtype", "pot_method")
    search_fields = ("name", "module__name", "layout", "pot_method")


class CategoryInline(admin.TabularInline):
    model = Category
    raw_id_fields = ("branch",)  # Too costly otherwise
    extra = 1


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ("name__name", "branch__module__name")
    list_display = ("__str__", "name", "release", "branch")


class ReleaseAdmin(admin.ModelAdmin):
    list_display = ("name", "status", "weight", "string_frozen")
    list_editable = ("weight",)
    inlines = (CategoryInline,)
    actions = (
        "copy_release",
        "delete_release",
    )

    def copy_release(self, request, queryset):
        """Copy an existing release and use master branches"""
        if not self.has_add_permission(request):
            raise PermissionDenied
        if queryset.count() > 1:
            messages.error(request, "Please copy only one release at a time")
            return HttpResponseRedirect(request.path)

        base_rel = queryset.first()
        with transaction.atomic():
            new_rel = Release.objects.create(
                name=base_rel.name + "-copy",
                description=base_rel.description + "-copy",
                string_frozen=False,
                status=base_rel.status,
            )

            branch_seen = set()
            for cat in base_rel.category_set.all():
                if not cat.branch.is_head:
                    mod = Module.objects.get(pk=cat.branch.module.id)
                    branch = mod.get_head_branch()
                else:
                    branch = cat.branch
                if branch in branch_seen:
                    continue
                Category.objects.create(release=new_rel, branch=branch, name=cat.name)
                branch_seen.add(branch)
        messages.success(request, f"New release '{new_rel.name}' created")
        return HttpResponseRedirect(request.path)

    copy_release.short_description = "Copy release (and associated branches)"

    def delete_release(self, request, queryset):
        """Admin action to delete releases *with* branches which are not linked to another release"""
        if not self.has_delete_permission(request):
            raise PermissionDenied
        if request.POST.get("post"):
            # Already confirmed
            for obj in queryset:
                self.log_deletion(request, obj, str(obj))
            n = queryset.count()
            b = 0
            for release in queryset:
                branches = Branch.objects.filter(category__release=release)
                for branch in branches:
                    if branch.releases.count() < 2 and not branch.is_head:
                        branch.delete()
                        b += 1
            queryset.delete()
            self.message_user(
                request,
                "Successfully deleted %(countr)d release(s) and %(countb)d branch(es)."
                % {
                    "countr": n,
                    "countb": b,
                },
            )
            # Return None to display the change list page again.
            return None
        context = {
            "title": "Are you sure?",
            "queryset": queryset,
            "app_label": self.model._meta.app_label,
            "model_label": self.model._meta.verbose_name_plural,
            "action_checkbox_name": helpers.ACTION_CHECKBOX_NAME,
        }
        return render(request, "admin/delete_release_confirmation.html", context)

    delete_release.short_description = "Delete release (and associated branches)"


class InformationInline(admin.TabularInline):
    model = Information
    extra = 0


class StatisticsAdmin(admin.ModelAdmin):
    search_fields = ("language__name", "branch__module__name")
    raw_id_fields = ("branch", "domain", "language", "full_po", "part_po")
    inlines = (InformationInline,)


class PoFileAdmin(admin.ModelAdmin):
    search_fields = ("path",)
    list_display = ("__str__", "filename", "updated", "tr_percentage")


admin.site.register(Statistics, StatisticsAdmin)
admin.site.register(PoFile, PoFileAdmin)
admin.site.register(Branch, BranchAdmin)
admin.site.register(Domain, DomainAdmin)
admin.site.register(CategoryName)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Module, ModuleAdmin)
admin.site.register(Release, ReleaseAdmin)
