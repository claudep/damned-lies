from django.db import migrations

CATEGORY_CHOICES = (
    ("default", "Default"),
    ("admin-tools", "Administration Tools"),
    ("dev-tools", "Development Tools"),
    ("desktop", "GNOME Desktop"),
    ("dev-platform", "GNOME Developer Platform"),
    ("proposed", "New Module Proposals"),
    ("g3-core", "Core"),
    ("g3-utils", "Utils"),
    ("g3-apps", "Apps"),
    ("g3-a11y", "Accessibility"),
    ("g3-games", "Games"),
    ("g3-backends", "Backends"),
    ("g3-core-libs", "Core Libraries"),
    ("g3-extra-libs", "Extra Libraries"),
    ("g2-legacy", "Legacy Desktop"),
    ("stable", "Stable Branches"),
    ("dev", "Development Branches"),
)


def migrate_categs(apps, schema_editor):
    Category = apps.get_model("stats", "Category")
    CategoryName = apps.get_model("stats", "CategoryName")

    for cat_key, cat_name in CATEGORY_CHOICES:
        cn = CategoryName.objects.create(name=cat_name)
        Category.objects.filter(name=cat_key).update(name_id=cn)
        if cat_name == "Development Tools":
            Category.objects.filter(name="g3-dev-tools").update(name_id=cn)


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0002_add_category_name"),
    ]

    operations = [
        migrations.RunPython(migrate_categs),
    ]
