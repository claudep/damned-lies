from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0006_add_domain_branch_from_to"),
    ]

    operations = [
        migrations.AlterField(
            model_name="module",
            name="bugs_base",
            field=models.CharField(max_length=250, null=True, blank=True),
        ),
    ]
