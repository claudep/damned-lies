from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0007_extend_bugs_base"),
    ]

    operations = [
        migrations.AddField(
            model_name="domain",
            name="extra_its_dirs",
            field=models.TextField(
                help_text="colon-separated directories containing extra .its/.loc files for gettext", blank=True
            ),
        ),
    ]
