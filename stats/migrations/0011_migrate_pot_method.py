from django.db import migrations


def migrate_pot_method(apps, schema_editor):
    Domain = apps.get_model("stats", "Domain")
    for domain in Domain.objects.exclude(pot_method_old=""):
        if domain.pot_method_old == ":":
            domain.pot_method = "in_repo"
        elif domain.pot_method_old == "<gettext>":
            domain.pot_method = "gettext"
        elif domain.pot_method_old == "<intltool>":
            domain.pot_method = "intltool"
        elif domain.pot_method_old.startswith("http"):
            domain.pot_method = "url"
            domain.pot_params = domain.pot_method_old
        else:
            domain.pot_method = "shell"
            domain.pot_params = domain.pot_method_old
        domain.save()


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0010_pot_method"),
    ]

    operations = [migrations.RunPython(migrate_pot_method)]
