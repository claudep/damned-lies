from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0012_remove_domain_pot_method_old"),
    ]

    operations = [
        migrations.AddField(
            model_name="domain",
            name="layout",
            field=models.CharField(
                default="",
                help_text="UI standard is 'po/{lang}.po', doc standard is 'po/{lang}/{lang}.po'",
                max_length=100,
            ),
            preserve_default=False,
        ),
    ]
