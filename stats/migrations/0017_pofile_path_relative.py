import os

from django.conf import settings
from django.db import migrations


def strip_path_prefix(apps, schema_editor):
    Statistics = apps.get_model("stats", "Statistics")
    scratch_dir = os.path.basename(str(settings.SCRATCHDIR))

    def strip_path(stat):
        old_path = stat.full_po.path
        stat.full_po.path = old_path.split("/" + scratch_dir + "/")[1]
        stat.full_po.save()
        if stat.part_po is not None and stat.part_po.path.startswith("/"):
            old_path = stat.part_po.path
            stat.part_po.path = old_path.split("/" + scratch_dir + "/")[1]
            stat.part_po.save()

    for stat in Statistics.objects.filter(full_po__isnull=False, full_po__path__startswith="/"):
        strip_path(stat)


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0016_removed_bugs_fields"),
    ]

    operations = [migrations.RunPython(strip_path_prefix, migrations.RunPython.noop)]
