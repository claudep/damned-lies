from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0017_pofile_path_relative"),
    ]

    operations = [
        migrations.RenameField(
            model_name="pofile",
            old_name="figures",
            new_name="figures_old",
        ),
        migrations.RenameField(
            model_name="branch",
            old_name="file_hashes",
            new_name="file_hashes_old",
        ),
        migrations.AddField(
            model_name="pofile",
            name="figures",
            field=models.JSONField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="branch",
            name="file_hashes",
            field=models.JSONField(blank=True, null=True, editable=False),
        ),
    ]
