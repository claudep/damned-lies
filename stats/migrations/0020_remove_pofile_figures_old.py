from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0019_migrate_old_custom_files"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="pofile",
            name="figures_old",
        ),
        migrations.RemoveField(
            model_name="branch",
            name="file_hashes_old",
        ),
    ]
