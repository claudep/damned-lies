from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0020_remove_pofile_figures_old"),
    ]

    operations = [
        migrations.AlterField(
            model_name="release",
            name="name",
            field=models.SlugField(max_length=20, unique=True),
        ),
    ]
