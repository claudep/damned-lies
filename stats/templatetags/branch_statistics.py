from collections import OrderedDict

from django import template

from languages.models import Language
from stats.models import Branch, StatisticsBase

register = template.Library()


@register.simple_tag()
def get_branch_ui_statistics(
    branch: Branch, mandatory_languages: tuple[Language] | None = ()
) -> OrderedDict[str, list["StatisticsBase"]]:
    return branch.get_ui_statistics(mandatory_languages)


@register.simple_tag()
def get_branch_documentation_statistics(
    branch: Branch, mandatory_languages: tuple[Language] | None = ()
) -> OrderedDict[str, list["StatisticsBase"]]:
    return branch.get_documentation_statistics(mandatory_languages)
