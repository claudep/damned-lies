import markdown as markdown_pkg
from django import template
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import get_language_bidi
from django.utils.translation import gettext as _

from damnedlies import logger
from stats.models import FakeLangStatistics, FakeSummaryStatistics, PoFile, Statistics

register = template.Library()

STATISTICS_FULL = (
    '<pre class="stats">'
    "<b>{prc:>3}%</b>"
    '<span class="num1"> {translated:>6}</span>'
    '<span class="num2"> {fuzzy:>5}</span>'
    '<span class="num3"> {untranslated:>5}</span>'
    "</pre>"
)

STATISTICS_SHORT = (
    '<span class="num1">{translated}</span>/'
    '<span class="num2">{fuzzy}</span>/'
    '<span class="num3">{untranslated}</span>'
)

PROGRESS_BAR = (
    '<div class="translated" style="width: {trans}px;"></div>'
    '<div class="fuzzy" style="{dir}: {trans}px; width: {fuzzy}px;"></div>'
    '<div class="untranslated" style="{dir}: {tr_fu}px; width: {untrans}px;"></div>'
)


@register.filter
def linked_with(value, arg) -> str:
    """This filter returns an object (passed in value) enclosed with its
    absolute url arg is the linked text"""
    return f"<a href='{value.get_absolute_url()}'>{arg}</a>"


@register.filter
def support_class(value) -> str:
    """Returns a class depending on the coverage of the translation stats.
    Value is a translation percentage"""
    if value >= 80:
        return "supported"
    if value >= 50:
        return "partially"
    return "not_supported"


@register.filter
def support_class_total(stats):
    """Returns a class depending on the trend."""
    actual = stats["stats"][-1]

    if stats["diff"] >= 0:
        return support_class(actual)
    if actual >= 80:
        return "partially"
    return "not_supported"


@register.filter
def escape_at_sign(value: str) -> str:
    """Replace '@' with '__', accepted sequence in JS ids."""
    return value.replace("@", "__")


@register.filter
def browse_bugs(module, content):
    return module.get_bugs_i18n_url(content)


@register.simple_tag
def num_stats_for_statistic(statistics: Statistics, scope: str = "full", for_words: bool = False) -> str:
    if not isinstance(statistics, Statistics | FakeLangStatistics | FakeSummaryStatistics):
        logger.error("The given statistic is not a Statistics object (is %s). Skipping.", type(statistics))
        return _render_error_num_stats_template()

    if for_words:
        translation_statistics = {
            "prc": statistics.tr_word_percentage(scope),
            "translated": statistics.translated_words(scope),
            "fuzzy": statistics.fuzzy_words(scope),
            "untranslated": statistics.untranslated_words(scope),
        }
    else:
        translation_statistics = {
            "prc": statistics.tr_percentage(scope),
            "translated": statistics.translated(scope),
            "fuzzy": statistics.fuzzy(scope),
            "untranslated": statistics.untranslated(scope),
        }
    return _render_statistics_template(translation_statistics, _show_zeros_from_scope(scope))


@register.simple_tag
def num_stats_for_pofile(statistics: PoFile, scope: str = "full", for_words: bool = False) -> str:
    if not isinstance(statistics, PoFile):
        logger.error("The given statistic is not a PoFile (is %s). Skipping.", type(statistics))
        return _render_error_num_stats_template()

    if for_words:
        translation_statistics = {
            "translated": statistics.translated_words,
            "fuzzy": statistics.fuzzy_words,
            "untranslated": statistics.untranslated_words,
        }
    else:
        translation_statistics = {
            "translated": statistics.translated,
            "fuzzy": statistics.fuzzy,
            "untranslated": statistics.untranslated,
        }
    if scope != "short":
        if for_words:
            translation_statistics["prc"] = statistics.tr_word_percentage()
        else:
            translation_statistics["prc"] = statistics.tr_percentage()
    return _render_statistics_template(translation_statistics, _show_zeros_from_scope(scope))


@register.simple_tag
def num_stats_for_dict_of_stats(statistics: dict, scope: str = "full"):
    if not isinstance(statistics, dict):
        logger.error("The given statistic is not a dictionary (is %s). Skipping.", type(statistics))
        return _render_error_num_stats_template()

    translation_statistics = statistics
    # FIXME: looks like a hotfix from Release.total_for_lang
    # For example, when a subset of the output of total_for_lang is used.
    if "translated_perc" in translation_statistics:
        translation_statistics["prc"] = translation_statistics["translated_perc"]
    return _render_statistics_template(translation_statistics, _show_zeros_from_scope(scope))


def _show_zeros_from_scope(scope: str) -> bool:
    show_zeros = False
    if "," in scope:
        scope, zeros = scope.split(",", 1)
        show_zeros = zeros == "zeros"
    return show_zeros


def _render_statistics_template(translation_statistics: dict, show_zeros: bool) -> str:
    if "prc" in translation_statistics:
        template_to_load = STATISTICS_FULL
        if not show_zeros:
            translation_statistics = {
                k: (" " if v == 0 and k != "prc" else v) for k, v in translation_statistics.items()
            }
    else:
        template_to_load = STATISTICS_SHORT
    return format_html(template_to_load, **translation_statistics)


def _render_error_num_stats_template() -> str:
    return mark_safe("""<span class="badge text-bg-danger">""" + _("Error") + "</span>")


@register.filter
def vis_stats(stat, scope="full"):
    """Produce visual stats with green/red bar"""
    bidi = "right" if get_language_bidi() else "left"
    if isinstance(stat, Statistics | FakeLangStatistics | FakeSummaryStatistics):
        trans = stat.tr_percentage(scope)
        fuzzy = stat.fu_percentage(scope)
        untrans = stat.un_percentage(scope)
    elif isinstance(stat, PoFile):
        trans = stat.tr_percentage()
        fuzzy = stat.fu_percentage()
        untrans = stat.un_percentage()
    elif isinstance(stat, dict):
        trans = stat["translated_perc"]
        fuzzy = stat["fuzzy_perc"]
        untrans = stat["untranslated_perc"]
    else:
        text = '<div class="untranslated" style="{dir}: 0px; width: 100px;"></div>'
        return format_html(text, dir=bidi)

    return format_html(
        PROGRESS_BAR,
        **{
            "dir": bidi,
            "trans": trans,
            "fuzzy": fuzzy,
            "tr_fu": trans + fuzzy,
            "untrans": untrans,
        },
    )


@register.filter
def vis_word_stats(stat, scope="full"):
    """Produce visual stats with green/red bar"""
    if isinstance(stat, Statistics | FakeLangStatistics | FakeSummaryStatistics):
        trans = stat.tr_word_percentage(scope)
        fuzzy = stat.fu_word_percentage(scope)
        untrans = stat.un_word_percentage(scope)
    elif isinstance(stat, PoFile):
        trans = stat.tr_word_percentage()
        fuzzy = stat.fu_word_percentage()
        untrans = stat.un_word_percentage()
    else:
        trans = stat["translated_perc"]
        fuzzy = stat["fuzzy_perc"]
        untrans = stat["untranslated_perc"]

    return format_html(
        PROGRESS_BAR,
        **{
            "dir": "right" if get_language_bidi() else "left",
            "trans": trans,
            "fuzzy": fuzzy,
            "tr_fu": trans + fuzzy,
            "untrans": untrans,
        },
    )


@register.filter
def is_video(fig):
    return fig["path"].endswith(".ogv")


@register.filter(is_safe=True)
def markdown(value, arg=""):
    """
    Copy of deprecated django.contrib.markup
    Runs Markdown over a given value, optionally using various
    extensions python-markdown supports.

    Syntax::

        {{ value|markdown:"extension1_name,extension2_name..." }}
    """
    extensions = [e for e in arg.split(",") if e]
    return mark_safe(markdown_pkg.markdown(value, extensions=extensions))
