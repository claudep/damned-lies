import shutil
import tarfile
import tempfile
from functools import wraps
from pathlib import Path
from unittest.mock import patch

from django.conf import settings


class PatchShellCommand:
    """
    Mock common.utils.run_shell_commands and gather all passed commands.
    `only` is an optional list of commands to limit mocking to (empty -> all).
    """

    def __init__(self, only=None):
        self.only = only

    def __enter__(self):
        self.cmds = []
        self.patcher1 = patch("stats.models.run_shell_command", side_effect=self.mocked_run_shell_command)
        self.patcher1.start()
        self.patcher2 = patch("stats.utils.run_shell_command", side_effect=self.mocked_run_shell_command)
        self.patcher2.start()
        self.patcher3 = patch("stats.repos.run_shell_command", side_effect=self.mocked_run_shell_command)
        self.patcher3.start()
        return self.cmds

    def mocked_run_shell_command(self, cmd, *args, **kwargs):
        cmd_str = " ".join(cmd) if isinstance(cmd, list) else cmd
        self.cmds.append(cmd_str)
        if self.only is not None and not any(needle in cmd_str for needle in self.only):
            # Pass the command to the real utils.run_shell_command
            from common.utils import run_shell_command

            return run_shell_command(cmd, *args, **kwargs)
        # Pretend the command was successful
        return 0, "", ""

    def __exit__(self, *args):
        self.patcher1.stop()
        self.patcher2.stop()
        self.patcher3.stop()


def test_scratchdir(test_func):
    """Decorator to temporarily use the scratchdir inside the test directory"""

    @wraps(test_func)
    def decorator(self):
        old_scratchdir = settings.SCRATCHDIR
        old_potdir = settings.POT_DIR
        settings.SCRATCHDIR = Path(tempfile.mkdtemp()) / "scratch"
        settings.POT_DIR = settings.SCRATCHDIR / "POT"
        settings.POT_DIR.mkdir(parents=True, exist_ok=True)
        settings.LOG_DIR = settings.SCRATCHDIR / "logs"
        settings.LOG_DIR.mkdir(parents=True, exist_ok=True)
        tar_path = Path(__file__).resolve().parent / "gnome-hello.tar.gz"
        with tarfile.open(tar_path) as gnome_hello_tar:
            gnome_hello_tar.extractall(settings.SCRATCHDIR / "git")
        try:
            test_func(self)
        finally:
            shutil.rmtree(settings.SCRATCHDIR)
            settings.SCRATCHDIR = old_scratchdir
            settings.POT_DIR = old_potdir

    return decorator
