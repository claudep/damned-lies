from django.contrib import admin
from django.forms import Field

from languages.models import Language
from teams.models import Role, Team


class LanguageInline(admin.TabularInline):
    model = Language
    # Languages are not supposed to be created in this form
    extra = 0


class RoleInline(admin.TabularInline):
    model = Role
    extra = 1


class TeamAdmin(admin.ModelAdmin):
    search_fields = ("name", "description")
    list_display = ("description", "use_workflow", "webpage_url")
    list_filter = ("use_workflow",)
    inlines = (LanguageInline, RoleInline)

    def formfield_for_dbfield(self: "TeamAdmin", db_field: type["Field"], **kwargs) -> type["Field"]:  # noqa ANN003
        # Reduced text area for aliases
        field = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "description":
            field.widget.attrs["rows"] = "4"
        return field


class RoleAdmin(admin.ModelAdmin):
    search_fields = (
        "person__first_name",
        "person__last_name",
        "person__username",
        "team__description",
        "role",
    )


admin.site.register(Team, TeamAdmin)
admin.site.register(Role, RoleAdmin)
