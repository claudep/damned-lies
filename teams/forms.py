from collections.abc import Generator
from typing import TYPE_CHECKING, Any, ClassVar

from django import forms
from django.conf import settings
from django.http import HttpRequest
from django.utils.translation import gettext as _

from common.utils import is_site_admin, send_mail
from teams.models import ROLE_CHOICES, Role, Team

if TYPE_CHECKING:
    from models import Person


class EditTeamDetailsForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ("webpage_url", "mailing_list", "mailing_list_subscribe", "use_workflow", "presentation")
        widgets: ClassVar[dict[str, Any]] = {
            "webpage_url": forms.TextInput(attrs={"size": 60, "class": "form-control"}),
            "mailing_list": forms.TextInput(attrs={"size": 60, "class": "form-control"}),
            "mailing_list_subscribe": forms.TextInput(attrs={"size": 60, "class": "form-control"}),
            "presentation": forms.Textarea(attrs={"cols": 60, "class": "form-control"}),
        }

    def __init__(self: "EditTeamDetailsForm", user: "Person", *args, **kwargs):  # noqa ANN003
        super().__init__(*args, **kwargs)
        self.user = user
        if is_site_admin(user):
            # Add coordinatorship dropdown
            all_members = [
                (r.id, r.person.name) for r in Role.objects.select_related("person").filter(team=self.instance)
            ]
            all_members.insert(0, ("", "-------"))
            try:
                current_coord_pk = Role.objects.filter(team=self.instance, role="coordinator")[0].pk
            except IndexError:
                current_coord_pk = None
            self.fields["coordinatorship"] = forms.ChoiceField(
                label=_("Coordinator"),
                choices=all_members,
                required=False,
                initial=current_coord_pk,
            )

    def save(self: "EditTeamDetailsForm", *args, **kwargs):  # noqa ANN003
        super().save(*args, **kwargs)
        if "coordinatorship" in self.changed_data and is_site_admin(self.user):
            # Change coordinator
            try:
                # Pass current coordinator as committer
                current_coord = Role.objects.filter(team=self.instance, role="coordinator")[0]
                current_coord.role = "committer"
                current_coord.save()
            except IndexError:
                pass
            if self.cleaned_data["coordinatorship"]:
                new_coord = Role.objects.get(pk=self.cleaned_data["coordinatorship"])
                new_coord.role = "coordinator"
                new_coord.save()


class EditMemberRoleForm(forms.Form):
    def __init__(self: "EditMemberRoleForm", roles: list, *args, **kwargs):  # noqa ANN003
        super().__init__(*args, **kwargs)
        choices = [x for x in ROLE_CHOICES if x[0] != "coordinator"]
        choices.extend([("inactivate", _("Mark as Inactive")), ("remove", _("Remove From Team"))])
        for role in roles:
            self.fields[str(role.pk)] = forms.ChoiceField(
                choices=choices,
                label=f'<a href="{role.person.get_absolute_url()}">{role.person.name}</a>',
                initial=role.role,
            )
        if roles:
            self.fields["form_type"] = forms.CharField(widget=forms.HiddenInput, initial=roles[0].role)

    def get_fields(self: "EditMemberRoleForm") -> Generator[Any, None, None]:
        for key in self.fields:
            if key not in {
                "form_type",
            }:
                yield self[key]

    def save(self: "EditMemberRoleForm", request: HttpRequest) -> None:  # noqa: ARG002
        for key, field in self.fields.items():
            form_value = self.cleaned_data[key]
            if field.initial != form_value:
                role = Role.objects.get(pk=key)
                if form_value == "remove":
                    team = role.team.description
                    role.delete()
                    message = _("You have been removed from the %(team)s team on %(site)s") % {
                        "team": team,
                        "site": settings.SITE_DOMAIN,
                    }
                    message += "\n\n" + _("This is an automatic message sent from %(site)s. Please do not answer.") % {
                        "site": settings.SITE_DOMAIN
                    }
                    send_mail(_("Removed from team"), message, to=[role.person.email])
                elif form_value == "inactivate":
                    role.is_active = False
                    role.save()
                else:
                    role.role = form_value
                    role.save()
                    message = _("Your role in the %(team)s team on %(site)s has been set to “%(role)s”") % {
                        "team": role.team.description,
                        "site": settings.SITE_DOMAIN,
                        "role": role.get_role_display(),
                    }
                    message += "\n\n" + _("This is an automatic message sent from %(site)s. Please do not answer.") % {
                        "site": settings.SITE_DOMAIN
                    }
                    send_mail(_("Role changed"), message, to=[role.person.email])
