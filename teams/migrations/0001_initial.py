from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("people", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Role",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                (
                    "role",
                    models.CharField(
                        default="translator",
                        max_length=15,
                        choices=[
                            ("translator", "Translator"),
                            ("reviewer", "Reviewer"),
                            ("committer", "Committer"),
                            ("coordinator", "Coordinator"),
                        ],
                    ),
                ),
                ("is_active", models.BooleanField(default=True)),
                ("person", models.ForeignKey(to="people.Person", on_delete=models.CASCADE)),
            ],
            options={
                "db_table": "role",
            },
        ),
        migrations.CreateModel(
            name="Team",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("name", models.CharField(max_length=80)),
                ("description", models.TextField()),
                ("use_workflow", models.BooleanField(default=True)),
                ("presentation", models.TextField(verbose_name="Presentation", blank=True)),
                ("webpage_url", models.URLField(null=True, verbose_name="Web page", blank=True)),
                (
                    "mailing_list",
                    models.EmailField(max_length=254, null=True, verbose_name="Mailing list", blank=True),
                ),
                ("mailing_list_subscribe", models.URLField(null=True, verbose_name="URL to subscribe", blank=True)),
                ("members", models.ManyToManyField(related_name="teams", through="teams.Role", to="people.Person")),
            ],
            options={
                "ordering": ("description",),
                "db_table": "team",
            },
        ),
        migrations.AddField(
            model_name="role",
            name="team",
            field=models.ForeignKey(to="teams.Team", on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name="role",
            unique_together=set([("team", "person")]),
        ),
    ]
