from django.urls import path, re_path

from teams import views

urlpatterns = [
    re_path(r"^(?P<format_requested>(xml|json))?/?$", views.teams, name="teams"),
    path("<locale:team_slug>/", views.team, name="team_slug"),
    path("<locale:team_slug>/edit/", views.team_edit, name="team_edit"),
    path("<locale:team_slug>/join/", views.connected_user_join_team, name="team_join"),
    path("<locale:team_slug>/leave/", views.connected_user_leave_team, name="team_leave"),
]
