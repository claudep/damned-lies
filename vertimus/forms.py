from collections.abc import Collection
from pathlib import Path
from typing import TYPE_CHECKING, Any

from django import forms
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _

from stats.models import Person
from stats.utils import check_po_conformity
from vertimus.models import Action, ActionCI, ActionSeparator

if TYPE_CHECKING:
    from vertimus.models import State


class DisabledLabel(str):
    """String subclass to mark some label as disabled."""


class DisablableSelect(forms.Select):
    """Custom widget to allow a Select option to be disabled."""

    def create_option(self: "DisablableSelect", *args, **kwargs) -> dict[str, Any]:  # noqa: ANN002, ANN003
        context = super().create_option(*args, **kwargs)
        if isinstance(context["label"], DisabledLabel):
            context["attrs"]["disabled"] = True
            if context["selected"]:
                context["selected"] = False
                del context["attrs"]["selected"]
        return context


class AuthorChoiceField(forms.ModelChoiceField):
    widget = DisablableSelect

    def label_from_instance(self: "AuthorChoiceField", person: Person) -> str:  # noqa: PLR6301
        if str(person) == person.username:
            return DisabledLabel(gettext("%(name)s (full name missing)") % {"name": person.username})
        if not person.email:
            return DisabledLabel(gettext("%(name)s (email missing)") % {"name": str(person)})
        return str(person)


class ActionForm(forms.Form):
    action = forms.ChoiceField(label=_("Action"), choices=(), widget=DisablableSelect)
    comment = forms.CharField(
        label=_("Comment"),
        max_length=5000,
        required=False,
        widget=forms.Textarea(attrs={"rows": 8, "cols": 70, "class": "form-control"}),
    )
    author = AuthorChoiceField(label=_("Commit author"), queryset=Person.objects.none(), required=False)
    sync_master = forms.BooleanField(required=False)
    file = forms.FileField(label=_("File"), required=False, help_text=_("Upload a .po, .gz, .bz2, .xz or .png file"))
    send_to_ml = forms.BooleanField(label=_("Send message to the team mailing list"), required=False)

    def __init__(
        self: "ActionForm",
        current_user: "Person",
        state: "State",
        actions: Collection[Action],
        *args,  # noqa: ANN002
        **kwargs,  # noqa: ANN003
    ) -> None:
        super().__init__(*args, **kwargs)
        self.actions = actions
        self.current_user = current_user
        self.fields["action"].choices = [
            (act.name, DisabledLabel(act.description) if isinstance(act, ActionSeparator) else act.description)
            for act in actions
        ]
        self.fields["action"].help_link = reverse("help", args=["vertimus_workflow", 1])
        if state and ActionCI in map(type, self.actions):
            self.fields["author"].queryset = state.involved_persons(extra_user=current_user).order_by(
                "last_name", "username"
            )
            self.fields["author"].initial = state.get_latest_po_file_action().person
        has_mailing_list = state and state.language and state.language.team and bool(state.language.team.mailing_list)
        if not has_mailing_list:
            del self.fields["send_to_ml"]
        if state and state.branch.is_head:
            del self.fields["sync_master"]
        elif state:
            main_branch = state.branch.module.get_head_branch()
            self.fields["sync_master"].label = _("Sync with %(name)s") % {"name": main_branch.name}
            self.fields["sync_master"].help_text = _("Try to cherry-pick the commit to the %(name)s branch") % {
                "name": main_branch.name
            }

    def clean_file(self: "ActionForm") -> dict[str, Any]:
        data = self.cleaned_data["file"]
        if data:
            ext = Path(data.name).suffix
            if ext not in {".po", ".gz", ".bz2", ".xz", ".png"}:
                raise ValidationError(_("Only files with extension .po, .gz, .bz2, .xz or .png are admitted."))
            # If this is a .po file, check validity (msgfmt)
            if ext == ".po":
                if check_po_conformity(data):
                    raise ValidationError(
                        _(".po file does not pass “msgfmt -vc”. Please correct the file and try again.")
                    )
        return data

    def clean(self: "ActionForm") -> dict[str, Any]:
        cleaned_data = self.cleaned_data
        action_code = cleaned_data.get("action")
        if action_code is None:
            raise ValidationError(_("Invalid action. Someone probably posted another action just before you."))
        if action_code == "CI":
            if not cleaned_data["author"]:
                raise ValidationError(_("Committing a file requires a commit author."))
            if "Token" in getattr(self.current_user, "backend", ""):
                raise ValidationError(_("Committing a file with token-based authentication is prohibited."))
        comment = cleaned_data.get("comment")
        file = cleaned_data.get("file")
        action = Action.new_by_name(action_code, comment=comment, file=file)

        if action.comment_is_required and not comment:
            raise ValidationError(_("A comment is needed for this action."))

        if self.is_valid() and action.arg_is_required and not comment and not file:
            raise ValidationError(_("A comment or a file is needed for this action."))

        if self.is_valid() and action.file_is_required and not file:
            raise ValidationError(_("A file is needed for this action."))

        if action.file_is_prohibited and file:
            raise ValidationError(_("Please, don’t send a file with a “Reserve” action."))

        return cleaned_data
