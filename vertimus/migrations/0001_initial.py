from django.db import migrations, models

import vertimus.models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "__first__"),
        ("people", "__first__"),
        ("languages", "__first__"),
    ]

    operations = [
        migrations.CreateModel(
            name="Action",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("name", models.SlugField(max_length=8)),
                ("created", models.DateTimeField(editable=False)),
                ("comment", models.TextField(null=True, blank=True)),
                ("file", models.FileField(null=True, upload_to=vertimus.models.generate_upload_filename, blank=True)),
                (
                    "merged_file",
                    models.OneToOneField(null=True, blank=True, to="stats.PoFile", on_delete=models.SET_NULL),
                ),
                ("person", models.ForeignKey(to="people.Person", on_delete=models.CASCADE)),
            ],
            options={
                "db_table": "action",
                "verbose_name": "action",
            },
        ),
        migrations.CreateModel(
            name="ActionArchived",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("name", models.SlugField(max_length=8)),
                ("created", models.DateTimeField(editable=False)),
                ("comment", models.TextField(null=True, blank=True)),
                ("file", models.FileField(null=True, upload_to=vertimus.models.generate_upload_filename, blank=True)),
                ("sequence", models.IntegerField(null=True)),
                (
                    "merged_file",
                    models.OneToOneField(null=True, blank=True, to="stats.PoFile", on_delete=models.SET_NULL),
                ),
                ("person", models.ForeignKey(to="people.Person", on_delete=models.CASCADE)),
            ],
            options={
                "db_table": "action_archived",
            },
        ),
        migrations.CreateModel(
            name="State",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("name", models.SlugField(default="None", max_length=20)),
                ("updated", models.DateTimeField(auto_now=True)),
                ("branch", models.ForeignKey(to="stats.Branch", on_delete=models.CASCADE)),
                ("domain", models.ForeignKey(to="stats.Domain", on_delete=models.CASCADE)),
                ("language", models.ForeignKey(to="languages.Language", on_delete=models.CASCADE)),
                ("person", models.ForeignKey(default=None, to="people.Person", null=True, on_delete=models.SET_NULL)),
            ],
            options={
                "db_table": "state",
                "verbose_name": "state",
            },
        ),
        migrations.AddField(
            model_name="actionarchived",
            name="state_db",
            field=models.ForeignKey(to="vertimus.State", on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name="action",
            name="state_db",
            field=models.ForeignKey(to="vertimus.State", on_delete=models.CASCADE),
        ),
        migrations.CreateModel(
            name="ActionAA",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="ActionCI",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="ActionIC",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="ActionRC",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="ActionRP",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="ActionRT",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="ActionTC",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="ActionTR",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="ActionUNDO",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="ActionUP",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="ActionUT",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="ActionWC",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.action",),
        ),
        migrations.CreateModel(
            name="StateCommitted",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.state",),
        ),
        migrations.CreateModel(
            name="StateCommitting",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.state",),
        ),
        migrations.CreateModel(
            name="StateNone",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.state",),
        ),
        migrations.CreateModel(
            name="StateProofread",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.state",),
        ),
        migrations.CreateModel(
            name="StateProofreading",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.state",),
        ),
        migrations.CreateModel(
            name="StateToCommit",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.state",),
        ),
        migrations.CreateModel(
            name="StateToReview",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.state",),
        ),
        migrations.CreateModel(
            name="StateTranslated",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.state",),
        ),
        migrations.CreateModel(
            name="StateTranslating",
            fields=[],
            options={
                "proxy": True,
            },
            bases=("vertimus.state",),
        ),
        migrations.AlterUniqueTogether(
            name="state",
            unique_together=set([("branch", "domain", "language")]),
        ),
    ]
