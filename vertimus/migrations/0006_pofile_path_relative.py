import os

from django.conf import settings
from django.db import migrations


def strip_path_prefix(apps, schema_editor):
    action = apps.get_model("vertimus", "Action")
    action_archived = apps.get_model("vertimus", "Action")
    media_dir = os.path.basename(settings.MEDIA_ROOT)

    def strip_path(action):
        old_path = action.merged_file.path
        action.merged_file.path = old_path.split("/" + media_dir + "/")[1]
        action.merged_file.save()

    for act in action.objects.filter(merged_file__isnull=False):
        strip_path(act)
    for act in action_archived.objects.filter(merged_file__isnull=False):
        strip_path(act)


class Migration(migrations.Migration):
    dependencies = [
        ("vertimus", "0005_action_proxy_pofile"),
    ]

    operations = [migrations.RunPython(strip_path_prefix, migrations.RunPython.noop)]
