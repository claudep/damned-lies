from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("people", "__first__"),
        ("vertimus", "0006_pofile_path_relative"),
    ]

    operations = [
        migrations.AlterField(
            model_name="action",
            name="person",
            field=models.ForeignKey(null=True, on_delete=models.deletion.SET_NULL, to="people.person"),
        ),
        migrations.AlterField(
            model_name="actionarchived",
            name="person",
            field=models.ForeignKey(null=True, on_delete=models.deletion.SET_NULL, to="people.person"),
        ),
    ]
