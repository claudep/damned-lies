from django.urls import path

from damnedlies.urls import module_branch_domain
from vertimus import views

urlpatterns = [
    path("<int:stats_id>/<int:lang_id>/", views.vertimus_by_stats_id, name="vertimus_by_stats_id"),
    path("<int:branch_id>/<int:domain_id>/<int:language_id>/", views.vertimus_by_ids, name="vertimus_by_ids"),
    path("diff/<int:action_id_1>/<int:action_id_2>/<int:level>/", views.vertimus_diff, name="vertimus_diff"),
    path(
        "uploads/%s/<locale:locale_name>)/latest/" % module_branch_domain,
        views.latest_uploaded_po,
        name="latest_uploaded_po",
    ),
    path(
        "%s/<locale:locale_name>/level<int:level>/" % module_branch_domain,
        views.vertimus_by_names,
        name="vertimus_archives_by_names",
    ),
    path("%s/<locale:locale_name>/" % module_branch_domain, views.vertimus_by_names, name="vertimus_by_names"),
    path("<locale:locale>/activity_summary/", views.activity_by_language, name="activity_by_language"),
    path("action/<int:action_pk>/qcheck/", views.QualityCheckView.as_view(), name="action-quality-check"),
    path("stats/<int:stats_pk>/qcheck/", views.QualityCheckView.as_view(), name="stats-quality-check"),
    path("stats/<int:stats_pk>/msgiddiff/", views.MsgiddiffView.as_view(), name="stats-msgiddiff"),
    path("action/<int:action_pk>/build_help/", views.BuildTranslatedDocsView.as_view(), name="action-build-help"),
]
